from dotenv import load_dotenv
from random import randint, choice, getrandbits
from ipaddress import IPv4Network, IPv4Address
from netaddr import IPNetwork, IPAddress
from bs4 import BeautifulSoup
from multiprocessing import Pool
from multiprocessing import freeze_support
import mysql.connector
import os
import socket
import sqlite3
import struct
import requests
import threading
import urllib3
import json

load_dotenv()

DATABASE_HOST = os.getenv("DATABASE_HOST")
DATABASE_USERNAME = os.getenv("DATABASE_USERNAME")
DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD")
DATABASE_DB = os.getenv("DATABASE_DB")

mydb = mysql.connector.connect(
  host=DATABASE_HOST,
  user=DATABASE_USERNAME,
  password=DATABASE_PASSWORD,
  database=DATABASE_DB
)

RANGES_FILE = "subnets/aws.lst"
TARGETS_FILE = "targets/aws.lst"
GEOIP_FILE = "subnets/aws.json"

with open(GEOIP_FILE, 'r') as f:
  geoIp = json.load(f)
  f.close()

urllib3.disable_warnings()

ranges = [line.rstrip('\n') for line in open(RANGES_FILE, 'r')]
targets = [line.rstrip('\n') for line in open(TARGETS_FILE, 'r')]

headers = {
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Language': 'en-US,en;q=0.9'
}

def extractPropertyOrNone(prop, dictionary):
    if((prop in dictionary) and (dictionary[prop] != None)):
        return dictionary[prop]
    else:
        return 'Unknown'

def extractASNPropertyOrNone(prop, dictionary):
    if(('asn' in dictionary) and (prop in dictionary['asn']) and (dictionary['asn'][prop] != None)):
        return dictionary['asn'][prop]
    else:
        return 'Unknown'

def getGeo(ip):
    ipAddress = IPAddress(ip)
    for ipRange in ranges:
        if(ipAddress) in IPNetwork(ipRange):
            return geoIp[ipRange]
    return None

def portIsOpen(ip):
    try:
        response = requests.get(f'http://{ip}', headers=headers, verify=False)
        if response.text:
            soup = BeautifulSoup(response.text, 'html.parser')
            title = soup.find('title').string
            lookUpResult = getGeo(ip)
            city = extractPropertyOrNone('city', lookUpResult)
            region = extractPropertyOrNone('region', lookUpResult)
            region_code = extractPropertyOrNone('region_code', lookUpResult)
            country_name = extractPropertyOrNone('country_name', lookUpResult)
            postal = extractPropertyOrNone('postal', lookUpResult)
            asn = extractASNPropertyOrNone('asn', lookUpResult)
            asn_name = extractASNPropertyOrNone('name', lookUpResult)
            asn_domain = extractASNPropertyOrNone('domain', lookUpResult)
            asn_type = extractASNPropertyOrNone('type', lookUpResult)
            print(f'[Hit] {ip}: {title}')
            mycursor = mydb.cursor()
            mycursor.execute("INSERT INTO hits (ip, port, title, city, region, region_code, country_name, postal, asn, asn_name, asn_domain, asn_type) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (str(ip), '80', str(title), str(city), str(region), str(region_code), str(country_name), str(postal), str(asn), str(asn_name), str(asn_domain), str(asn_type)))
            mydb.commit()
            return True
        else:
            print(f"[Closed] - {ip}")
            return False
    except Exception as e:
        print(e)
        return False

def main():
    numThreads = input("Thread Count: ")
    freeze_support()

    pool = Pool(int(numThreads))
    pool.map(portIsOpen, targets)

    pool.close()
    pool.join()

if __name__ == "__main__":
    main()
