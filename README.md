# Site Reaper Scanner

This repository contains the source code for the scanning portion of Site Reaper.

## To Do:
- Dynamically load GeoIP/Subnet/Target information
- Switch to MySQL/PostgreSQL (Ehanced Types, less optimization, support for storing IPv6 Addresses)
- Database enhancements (Represent IPs as )
- Re-Add Autoindex Scraper
- Sensitive file auto-detection